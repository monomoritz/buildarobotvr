﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager> {

	public bool isDebug = false;

	public delegate void OnGameOver();
	public event OnGameOver onGameOver;

	private void Awake()
	{

		var test = Instance;
	}

	void Update () {
	}

	public void GameOver()
	{
		if (onGameOver != null)
		{
			onGameOver();
		}
		// UI stuff
		Debug.Log("GameOver");
	}
}
