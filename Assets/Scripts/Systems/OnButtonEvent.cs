﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using VRTK;
using VRTK.UnityEventHelper;

[RequireComponent(typeof(VRTK_Button))]
public class OnButtonEvent : MonoBehaviour
{
	public List<UnityEvent> onDown = new List<UnityEvent>();
	VRTK_Button button;

	// Use this for initialization
	void Start()
	{
		button = GetComponent<VRTK_Button>();
	}

	private void OnEnable()
	{
		StartCoroutine(EOnEnable());
	}

	IEnumerator EOnEnable()
	{
		yield return null;
		button.Pushed += Button_Pushed;
	}

	private void OnDisable()
	{
		button.Pushed += Button_Pushed;
	}

	private void Button_Pushed(object sender, Control3DEventArgs e)
	{
		foreach (var item in onDown)
		{
			item.Invoke();
		}
	}
}