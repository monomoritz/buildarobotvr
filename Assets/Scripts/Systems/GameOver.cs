﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {

	public Light lightForGameOver;

	SteamVR_Fade fade;

	// Use this for initialization
	void Start () {
		if (GameManager.Instance)
			GameManager.Instance.onGameOver += Instance_onGameOver;
		fade = FindObjectOfType<SteamVR_Fade>();
	}

	private void Instance_onGameOver()
	{
		StartCoroutine(EGameOver());
	}

	private IEnumerator EGameOver()
	{
		float start = Time.time;
		float startInt = lightForGameOver.intensity;
		while (Time.time < start + 3f)
		{
			lightForGameOver.intensity = Mathf.Lerp(startInt, 1000f, (Time.time - start) / 3f);
			yield return null;
		}
		if (!fade)
		{
			fade = FindObjectOfType<SteamVR_Fade>();
		}
		fade.OnStartFade(Color.black, 2f, false);
		yield return new WaitForSeconds(5f);
		SceneManager.CreateScene("Temp");
		SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().name);
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

	// Update is called once per frame
	void Update () {
		
	}
}
