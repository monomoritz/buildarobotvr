﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snapper : MonoBehaviour {

	public Transform snapped;
	public bool weldables = false;

	public void Snap(Transform t)
	{
		if (t != null)
		{
			snapped = t;
		}
	}
	
	public void Unsnap(Transform t)
	{
		if (t == snapped)
		{
			snapped = null;
		}
	}
}
