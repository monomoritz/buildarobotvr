﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK.GrabAttachMechanics;
using VRTK;
using System;

[RequireComponent(typeof(VRTK_BaseGrabAttach))]
[RequireComponent(typeof(VRTK_InteractableObject))]
public class FreeSnapable : MonoBehaviour {

	/// <summary>
	/// Are we snapped or not?
	/// </summary>
	protected bool isSnapped = false;
	/// <summary>
	/// Are we attempting to snap
	/// </summary>
	protected bool isSnapping = false;
	/// <summary>
	/// The object we are snapping to (the robot)
	/// </summary>
	public GameObject snapper;
	/// <summary>
	/// The controller currently grabbing us
	/// </summary>
	protected GameObject controller;
	/// <summary>
	/// The snappoint of this object
	/// </summary>
	protected Transform snapPoint;
	protected Vector3 snapPointPos;
	protected RaycastHit hitOnSnapper;

	protected VRTK_InteractableObject interactable;
	protected bool raycastHitting;

	#region EventMethods
	private void OnGrabbed(object sender, InteractableObjectEventArgs e)
	{
		controller = e.interactingObject;
		if (controller.GetComponent<OnTriggerMessage>())
		{
			controller.GetComponent<OnTriggerMessage>().onTriggerEnterEvent += ControllerTriggerEnter;
			controller.GetComponent<OnTriggerMessage>().onTriggerExitEvent += ControllerTriggerExit;
		}
			
	}

	private void OnUngrabbed(object sender, InteractableObjectEventArgs e)
	{
		if (!isSnapping)
		{
			if (controller.GetComponent<OnTriggerMessage>())
			{
				controller.GetComponent<OnTriggerMessage>().onTriggerEnterEvent -= ControllerTriggerEnter;
				controller.GetComponent<OnTriggerMessage>().onTriggerExitEvent -= ControllerTriggerExit;
			}
			if (isSnapped)
			{
				Unsnap();
			}
			controller = null;
		}
	}

	private void ControllerTriggerEnter(Collider other)
	{
		if (other.GetComponent<Snapper>())
		{
			Snap(other.gameObject);
		}
	}

	private void ControllerTriggerExit(Collider other)
	{
		if (other.gameObject == snapper)
		{
			Unsnap();
		}
	}
	#endregion

#region MonoMethods
	protected virtual void Start()
	{
		// References
		interactable = GetComponent<VRTK_InteractableObject>();
		
		// Get the point were snapping with
		if (transform.childCount > 0)
		{
			snapPoint = transform.GetChild(0);
			snapPointPos = snapPoint.localPosition;
		} else
		{
			snapPoint = transform;
			snapPointPos = Vector3.zero;
		}
		
	}

	private void OnEnable()
	{
		StartCoroutine(EOnEnable()); // Wait a frame until Unity got its shit together
	}

	IEnumerator EOnEnable()
	{
		yield return null;
		interactable.InteractableObjectGrabbed += OnGrabbed;
		interactable.InteractableObjectUngrabbed += OnUngrabbed;
	}

	private void OnDisable()
	{
		interactable.InteractableObjectGrabbed -= OnGrabbed;
		interactable.InteractableObjectUngrabbed -= OnUngrabbed;
	}

	// Update is called once per frame
	protected virtual void FixedUpdate () {
		raycastHitting = false;
		if (isSnapped)
		{
			// Get our raycast hit to reorient the object on the snapper surface
			Ray ray = new Ray(controller.transform.position, snapper.transform.position - controller.transform.position);
			Debug.DrawRay(ray.origin, ray.direction * 2f);
			int layerMask = 1 << 13;
			if (Physics.Raycast(ray, out hitOnSnapper, ray.direction.magnitude * 2f, layerMask))
			{
				raycastHitting = true;
			}
		}
	}

	private void LateUpdate()
	{
		if (raycastHitting && snapper) // If we're snapped and the raycast is hitting, adjust
		{
			//Debug.Log("Raycast is hitting");
			snapPoint.position = hitOnSnapper.point;
			snapPoint.up = snapPoint.position - snapper.transform.position;
			//Debug.Break();
		}
	}
	#endregion

	#region Methods
	protected virtual void Snap(GameObject objectToSnapTo)
	{
		if (!CanSnap())
			return;

		isSnapping = true;
		snapper = objectToSnapTo;

		interactable.ForceStopInteracting();
		interactable.isGrabbable = false;

		//Debug.Log("SnapStart @ " + Time.frameCount);
		StartCoroutine(AfterSnap());
	}

	protected virtual IEnumerator AfterSnap()
	{
		yield return null; // Wait a frame so VRTK did all its stuff and we can use the object again
		if (snapper)
		{
			//Debug.Log("Snap @ " + Time.frameCount);

			// Make the object not fall anymore
			var rb = GetComponent<Rigidbody>();
			if (rb)
			{
				rb.isKinematic = true;
			}
			// Disable collider so i doesnt collide with the snapper
			/*var co = GetComponent<Collider>();
			if (co)
			{
				co.isTrigger = true;
			}*/

			// Make everything a child of the snapper
			snapPoint.SetParent(snapper.transform, true);
			transform.SetParent(snapPoint, true);

			// Let everyone else know were done
			isSnapped = true;
			isSnapping = false;
			//Debug.Log("Snapped!");
		}
		else if (GameManager.Instance.isDebug)
		{
			// In very rare occurences we dont have a snapper after we tried snapping, if this happens, abort.
			Unsnap();
			Debug.LogWarning("1 frame after snap is no snapper.");
		}
	}

	public virtual void Unsnap()
	{
		Debug.Log("Try unsnap");
		if (!CanUnsnap())
			return;

		// Unchild everything
		transform.SetParent(null, true);
		snapPoint.SetParent(transform, true);

		var rb = GetComponent<Rigidbody>();
		if (rb)
		{
			rb.isKinematic = false;
			rb.ResetInertiaTensor();
		}
		var co = GetComponent<Collider>();
		if (co)
		{
			co.isTrigger = false;
		}

		interactable.isGrabbable = true;
		if (controller.GetComponent<VRTK_InteractGrab>().IsGrabButtonPressed())
		{
			transform.position = controller.transform.position;
			snapPoint.localPosition = snapPointPos;
			controller.GetComponent<VRTK_InteractTouch>().ForceStopTouching();
			controller.GetComponent<VRTK_InteractTouch>().ForceTouch(interactable.gameObject);
			controller.GetComponent<VRTK_InteractGrab>().AttemptGrab();
		}

		snapper = null;
		isSnapping = false;
		isSnapped = false;
		//Debug.Log("Unsnapped!");
	}

	private bool CanSnap()
	{
		bool snap = false;
		snap = interactable.IsGrabbed() && !isSnapped;
		return snap;
	}

	bool CanUnsnap()
	{
		bool unsnap = false;
		unsnap = isSnapped;
		return unsnap;
	}
#endregion
}
