﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class SimpleSnapable : Snapable {

	bool grabDown = false;
	bool holdingAfterDetach = false;

	protected override void Start()
	{
		base.Start();
	}

	private void GrabPressed(object sender, ControllerInteractionEventArgs e)
	{
		grabDown = true;
	}

	protected override void FixedUpdate()
	{
		if (holdingAfterDetach
			&& (!grabber.GetComponent<VRTK_InteractGrab>().IsGrabButtonPressed()
			|| Vector3.Distance(lastSnapper.position, grabber.transform.position) > distanceToDetach))
		{
			holdingAfterDetach = false;
		}
		if (snapper == null) // Check if we are snapped already
		{
			if (interactable.IsGrabbed() && !holdingAfterDetach) // Check if this object is grabbed
			{
				var s = FindObjectsOfType<Snapper>(); // Change this some time to a manager 
				foreach (var ss in s)
				{
					float distanceToAttachPoint = Vector3.Distance(ss.transform.position, snapPoint.position); // Check if a snapper is close to our snap point
					if (ss != null && distanceToAttachPoint < distanceToAttach)
					{
						Snap(ss.transform); // Go snap
					}
				}
			}
		}
		else
		{
			var rb = GetComponent<Rigidbody>();
			if (rb)
			{
				rb.isKinematic = true;
			}
			float distanceToAttachPoint = Vector3.Distance(snapper.position, grabber.transform.position);
			if (grabDown && distanceToAttachPoint > distanceToAttach && distanceToAttachPoint < distanceToDetach)
			{
				Unsnap();
				holdingAfterDetach = true;
			}
		}
		grabDown = false;
	}

	protected override void Snap(Transform objectToSnapTo)
	{
		base.Snap(objectToSnapTo);
		grabber.GetComponent<VRTK_InteractGrab>().GrabButtonPressed += GrabPressed;
	}

	public override void Unsnap()
	{
		base.Unsnap();
		grabber.GetComponent<VRTK_InteractGrab>().GrabButtonPressed -= GrabPressed;
	}
}
