﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

[RequireComponent(typeof(VRTK_InteractableObject))]
public class Lightswitch : MonoBehaviour {

	new public GameObject light;
	public bool on = true;
	Color startColor;

	// Use this for initialization
	void Start () {
		GetComponent<VRTK_InteractableObject>().InteractableObjectUsed += Lightswitch_InteractableObjectUsed;
		startColor = light.GetComponent<Renderer>().material.GetColor("_EmissionColor");
		on = startColor.grayscale > 0.5f;
	}

	private void Lightswitch_InteractableObjectUsed(object sender, InteractableObjectEventArgs e)
	{
		if (on)
			light.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.black);
		else
			light.GetComponent<Renderer>().material.SetColor("_EmissionColor", startColor);
		on = !on;
	}

	private void OnDisable()
	{
		GetComponent<VRTK_InteractableObject>().InteractableObjectUsed -= Lightswitch_InteractableObjectUsed;
	}
}
