﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using VRTK;

public class FreeWeldable : FreeSnapable {

	public float weldValue = 0f;
	public float weldPerSecond = 0.3f;
	public bool isFixed = false;
	public ParticleSystem sparks;

	public void Weld(GameObject weldController)
	{
		if (IsSnapped() && CanWeld())
		{
			weldValue = Mathf.Clamp01(weldValue + weldPerSecond * Time.deltaTime); // More weldvalue
			if (weldValue >= 1f)
			{
				Unsnap();
			}
			if (sparks)
			{
				sparks.Emit(1); // Sparkle sparkle
				sparks.Play();
			}
		}
	}

	protected override void FixedUpdate()
	{
		raycastHitting = false;
		if (weldValue < 1f) {
			base.FixedUpdate();
		}
	}

	protected override void Snap(GameObject objectToSnapTo)
	{
		base.Snap(objectToSnapTo);
	}

	public override void Unsnap()
	{
		if (weldValue < 1f) // If we welded good, we perma snap
		{
			base.Unsnap();
			isFixed = false;
			weldValue = 0f;
		} else
		{
			isFixed = true;
		}
	}

	public bool IsSnapped()
	{
		return isSnapped;
	}

	public bool CanWeld()
	{
		return weldValue < 1f;
	}
}
