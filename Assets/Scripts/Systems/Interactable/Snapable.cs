﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK.GrabAttachMechanics;
using VRTK;

[RequireComponent(typeof(VRTK_BaseGrabAttach))]
[RequireComponent(typeof(VRTK_InteractableObject))]
public class Snapable : MonoBehaviour {

	public Transform snapPoint;
	public Transform snapper = null;
	public float distanceToAttach = 0.1f;
	public float distanceToDetach = 0.3f;

	protected VRTK_BaseGrabAttach grabable;
	protected VRTK_InteractableObject interactable;
	protected GameObject grabber;
	protected Transform lastSnapper;

	protected virtual void Start () {
		grabable = GetComponent<VRTK_BaseGrabAttach>();
		interactable = GetComponent<VRTK_InteractableObject>();
	}
	
	protected virtual void FixedUpdate () {
		if (snapper == null) // Check if we are snapped already
		{
			if (interactable.IsGrabbed()) // Check if this object is grabbed
			{
				var s = FindObjectsOfType<Snapper>(); // Change this some time to a manager 
				foreach (var ss in s)
				{
					float distanceToAttachPoint = Vector3.Distance(ss.transform.position, snapPoint.position); // Check if a snapper is close to our snap point
					if (ss != null && distanceToAttachPoint < distanceToAttach)
					{
						Snap(ss.transform); // Go snap
					}
				}
			}
		}
		else
		{
			float distanceToAttachPoint = Vector3.Distance(snapper.position, grabber.transform.position);
			if (!grabber.GetComponent<VRTK_InteractGrab>().IsGrabButtonPressed() || distanceToAttachPoint > distanceToDetach)
			{
				Unsnap();
			}
		}
	}

	protected virtual void Snap(Transform objectToSnapTo)
	{
		if (objectToSnapTo == null)
			return;

		snapper = objectToSnapTo;

		grabber = interactable.GetGrabbingObject();
		interactable.ForceStopInteracting();

		var rb = GetComponent<Rigidbody>();
		if (rb)
		{
			rb.isKinematic = true;
		}
		var co = GetComponent<Collider>();
		if (co)
		{
			co.isTrigger = true;
		}

		StartCoroutine(AfterSnap());
		interactable.isGrabbable = false;
	}

	protected virtual IEnumerator AfterSnap()
	{
		yield return null;
		if (snapper)
		{
			snapPoint.SetParent(snapper, true);
			transform.SetParent(snapPoint, true);
			snapPoint.position = snapper.position;
			snapPoint.rotation = snapper.rotation;
		} else if (GameManager.Instance.isDebug)
		{
			Debug.Log("1 frame after snap is no snapper.");
		}
	}

	public virtual void Unsnap()
	{
		transform.SetParent(null, true);
		snapPoint.SetParent(null, true);
		snapPoint.SetParent(transform, true);

		var rb = GetComponent<Rigidbody>();
		if (rb)
		{
			rb.isKinematic = false;
			rb.ResetInertiaTensor();
		}
		var co = GetComponent<Collider>();
		if (co)
		{
			co.isTrigger = false;
		}

		interactable.isGrabbable = true;
		if (grabber.GetComponent<VRTK_InteractGrab>().IsGrabButtonPressed())
		{
			grabber.GetComponent<VRTK_InteractTouch>().ForceTouch(grabable.gameObject);
			grabber.GetComponent<VRTK_InteractGrab>().AttemptGrab();
		}

		lastSnapper = snapper;
		snapper = null;
	}
}
