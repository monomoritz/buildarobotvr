﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class SnapablePlate : SimpleSnapable {

	public bool isOberPlatte = false;
	public List<GameObject> oberPlatten = new List<GameObject>();
	public List<GameObject> mittelPlatten = new List<GameObject>();
	bool isSnapped = false;

	protected override void Snap(Transform objectToSnapTo)
	{
		if (isSnapped)
			return;
		isSnapped = true;
		base.Snap(objectToSnapTo);
		List<GameObject> platten = new List<GameObject>();
		if (isOberPlatte)
		{
			platten = oberPlatten;
		} else
		{
			platten = mittelPlatten;
		}
		KeyValuePair<GameObject, float> newPlate = new KeyValuePair<GameObject, float>(null, Mathf.Infinity);
		foreach (var item in platten)
		{
			float d = Vector3.Distance(transform.position, item.transform.position); // Distance between this plate and a plate on the robot
			if (!item.activeInHierarchy && (d < newPlate.Value)) // Check if distance is less than the others and if the exisiting one is already active
			{
				KeyValuePair<GameObject, float> p = new KeyValuePair<GameObject, float>(item, d);
				newPlate = p;
			}
		}
		newPlate.Key.SetActive(true);
		Destroy(gameObject);
	}

	public override void Unsnap()
	{
	}
}
