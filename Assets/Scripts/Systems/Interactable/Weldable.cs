﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using VRTK;

public class Weldable : Snapable {

	public float weldValue = 0f;
	public float weldPerSecond = 0.3f;
	public bool isFixed = false;
	public ParticleSystem sparks;

	public void Weld(GameObject weldController)
	{
		if (IsSnapped() && CanWeld())
		{
			weldValue = Mathf.Clamp01(weldValue + weldPerSecond * Time.deltaTime); // More weldvalue
			sparks.Emit(1); // Sparkle sparkle
			sparks.Play();
		}
	}

	public override void Unsnap()
	{
		if (weldValue < 1f) // If we welded good, we perma snap
		{
			base.Unsnap();
			isFixed = false;
			weldValue = 0f;
		} else
		{
			isFixed = true;
			snapPoint.SetParent(snapper, true);
			transform.SetParent(snapPoint, true);
		}
	}

	public bool IsSnapped()
	{
		return snapper;
	}

	public bool CanWeld()
	{
		return weldValue < 1f;
	}
}
