﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTriggerMessage : MonoBehaviour {

	public delegate void OnTriggerEnterEvent(Collider other);
	public event OnTriggerEnterEvent onTriggerEnterEvent;
	public delegate void OnTriggerStayEvent(Collider other);
	public event OnTriggerStayEvent onTriggerStayEvent;
	public delegate void OnTriggerExitEvent(Collider other);
	public event OnTriggerExitEvent onTriggerExitEvent;

	public delegate void OnCollisionEnterEvent(Collision other);
	public event OnCollisionEnterEvent onCollisionEnterEvent;
	public delegate void OnCollisionStayEvent(Collision other);
	public event OnCollisionStayEvent onCollisionStayEvent;
	public delegate void OnCollisionExitEvent(Collision other);
	public event OnCollisionExitEvent onCollisionExitEvent;

	private void OnTriggerEnter(Collider other)
	{
		if (onTriggerEnterEvent != null)
		{
			onTriggerEnterEvent(other);
			if (GameManager.Instance && GameManager.Instance.isDebug)
			{
				Debug.Log("OnTriggerEnter on " + name + " with " + other.name);
			}
		}
	}

	private void OnTriggerStay(Collider other)
	{
		if (onTriggerStayEvent != null)
		{
			onTriggerStayEvent(other);
			if (GameManager.Instance.isDebug)
			{
				Debug.Log("OnTriggerStay on " + name + " with " + other.name);
			}
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (onTriggerExitEvent != null)
		{
			onTriggerExitEvent(other);
			if (GameManager.Instance.isDebug)
			{
				Debug.Log("OnTriggerExit on " + name + " with " + other.name);
			}
		}
	}

	private void OnCollisionEnter(Collision collision)
	{
		if (onCollisionEnterEvent != null)
		{
			onCollisionEnterEvent(collision);
			if (GameManager.Instance.isDebug)
			{
				Debug.Log("OnCollisionEnter on " + name + " with " + collision.gameObject.name);
			}
		}
	}

	private void OnCollisionStay(Collision collision)
	{
		if (onCollisionStayEvent != null)
		{
			onCollisionStayEvent(collision);
			if (GameManager.Instance.isDebug)
			{
				Debug.Log("OnCollisionStay on " + name + " with " + collision.gameObject.name);
			}
		}
	}

	private void OnCollisionExit(Collision collision)
	{
		if (onCollisionExitEvent != null)
		{
			onCollisionExitEvent(collision);
			if (GameManager.Instance.isDebug)
			{
				Debug.Log("OnCollisionExit on " + name + " with " + collision.gameObject.name);
			}
		}
	}
}
