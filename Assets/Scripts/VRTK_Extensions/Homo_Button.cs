﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class Homo_Button : VRTK_Button {

	protected override bool DetectSetup()
	{
		finalDirection = (direction == ButtonDirection.autodetect ? DetectDirection() : direction);
		if (finalDirection == ButtonDirection.autodetect)
		{
			activationDir = Vector3.zero;
			return false;
		}
		if (direction != ButtonDirection.autodetect)
		{
			activationDir = CalculateActivationDir();
		}

		if (buttonForce)
		{
			buttonForce.relativeForce = GetForceVector();
		}

		if (Application.isPlaying)
		{
			buttonJoint = GetComponent<ConfigurableJoint>();

			bool recreate = false;
			Rigidbody oldBody = null;
			Vector3 oldAnchor = Vector3.zero;
			Vector3 oldAxis = Vector3.zero;
			SoftJointLimitSpring limitSpring = new SoftJointLimitSpring();
			bool oldConfiguredInWS = false;
			JointProjectionMode oldProjectionMode = new JointProjectionMode();
			float oldProjectionDistance = 0.1f;

			if (buttonJoint)
			{
				// save old values, needs to be recreated
				oldBody = buttonJoint.connectedBody;
				oldAnchor = buttonJoint.anchor;
				oldAxis = buttonJoint.axis;
				limitSpring = buttonJoint.linearLimitSpring;
				oldConfiguredInWS = buttonJoint.configuredInWorldSpace;
				oldProjectionMode = buttonJoint.projectionMode;
				oldProjectionDistance = buttonJoint.projectionDistance;
				
				DestroyImmediate(buttonJoint);
				recreate = true;
			}

			// since limit applies to both directions object needs to be moved halfway to activation before adding joint
			transform.position = transform.position + ((activationDir.normalized * activationDistance) * 0.5f);
			buttonJoint = gameObject.AddComponent<ConfigurableJoint>();

			if (recreate)
			{
				buttonJoint.connectedBody = oldBody;
				buttonJoint.anchor = oldAnchor;
				buttonJoint.axis = oldAxis;
				buttonJoint.configuredInWorldSpace = oldConfiguredInWS;
				buttonJoint.linearLimitSpring = limitSpring;
				buttonJoint.projectionMode = oldProjectionMode;
				buttonJoint.projectionDistance = oldProjectionDistance;
			}
			if (connectedTo)
			{
				buttonJoint.connectedBody = connectedTo.GetComponent<Rigidbody>();
			}

			SoftJointLimit buttonJointLimits = new SoftJointLimit();
			buttonJointLimits.limit = activationDistance * 0.501f; // set limit to half (since it applies to both directions) and a tiny bit larger since otherwise activation distance might be missed
			buttonJoint.linearLimit = buttonJointLimits;

			buttonJoint.angularXMotion = ConfigurableJointMotion.Locked;
			buttonJoint.angularYMotion = ConfigurableJointMotion.Locked;
			buttonJoint.angularZMotion = ConfigurableJointMotion.Locked;
			buttonJoint.xMotion = ConfigurableJointMotion.Locked;
			buttonJoint.yMotion = ConfigurableJointMotion.Locked;
			buttonJoint.zMotion = ConfigurableJointMotion.Locked;

			switch (finalDirection)
			{
				case ButtonDirection.x:
				case ButtonDirection.negX:
					if (Mathf.RoundToInt(Mathf.Abs(transform.right.x)) == 1)
					{
						buttonJoint.xMotion = ConfigurableJointMotion.Limited;
					}
					else if (Mathf.RoundToInt(Mathf.Abs(transform.up.x)) == 1)
					{
						buttonJoint.yMotion = ConfigurableJointMotion.Limited;
					}
					else if (Mathf.RoundToInt(Mathf.Abs(transform.forward.x)) == 1)
					{
						buttonJoint.zMotion = ConfigurableJointMotion.Limited;
					}
					break;
				case ButtonDirection.y:
				case ButtonDirection.negY:
					if (Mathf.RoundToInt(Mathf.Abs(transform.right.y)) == 1)
					{
						buttonJoint.xMotion = ConfigurableJointMotion.Limited;
					}
					else if (Mathf.RoundToInt(Mathf.Abs(transform.up.y)) == 1)
					{
						buttonJoint.yMotion = ConfigurableJointMotion.Limited;
					}
					else if (Mathf.RoundToInt(Mathf.Abs(transform.forward.y)) == 1)
					{
						buttonJoint.zMotion = ConfigurableJointMotion.Limited;
					}
					break;
				case ButtonDirection.z:
				case ButtonDirection.negZ:
					if (Mathf.RoundToInt(Mathf.Abs(transform.right.z)) == 1)
					{
						buttonJoint.xMotion = ConfigurableJointMotion.Limited;
					}
					else if (Mathf.RoundToInt(Mathf.Abs(transform.up.z)) == 1)
					{
						buttonJoint.yMotion = ConfigurableJointMotion.Limited;
					}
					else if (Mathf.RoundToInt(Mathf.Abs(transform.forward.z)) == 1)
					{
						buttonJoint.zMotion = ConfigurableJointMotion.Limited;
					}
					break;
			}
		}

		return true;
	}
}
