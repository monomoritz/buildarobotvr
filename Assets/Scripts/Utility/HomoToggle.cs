﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomoToggle : MonoBehaviour {

	public List<MonoBehaviour> behaviours = new List<MonoBehaviour>();

	public void Toggle()
	{
		foreach (var item in behaviours)
		{
			Debug.Log("Toggle");
			item.enabled = !item.enabled;
		}
	}
}
