﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//420 blazeit
[RequireComponent(typeof(ConfigurableJoint))]
public class ButtonToggleJoint : MonoBehaviour {

	ConfigurableJoint joint;
	Rigidbody rb;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody>();
		joint = GetComponent<ConfigurableJoint>();
	}
	
	public void Toggle()
	{
		rb.isKinematic = !rb.isKinematic;
		/*if (joint.angularXMotion == ConfigurableJointMotion.Free)
		{
			joint.angularXMotion = ConfigurableJointMotion.Locked;
			joint.angularYMotion = ConfigurableJointMotion.Locked;
			joint.angularZMotion = ConfigurableJointMotion.Locked;
		} else
		{
			joint.angularXMotion = ConfigurableJointMotion.Free;
			joint.angularYMotion = ConfigurableJointMotion.Free;
			joint.angularZMotion = ConfigurableJointMotion.Free;
		}*/
	}
}
