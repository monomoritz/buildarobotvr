﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(Snapper))]
public class RobotPlatten : MonoBehaviour {

	public List<GameObject> platten = new List<GameObject>();
	public bool weldables = false;

	private void FixedUpdate()
	{
		if(!weldables && platten.TrueForAll(x => x.activeInHierarchy))
		{
			weldables = false;
			GetComponent<Snapper>().weldables = true;
		}
	}
}
