﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotCore : MonoBehaviour {

	public TextMesh temperatureDisplay;
	public string tempString = " °C";
	public List<AnimationCurve> colorCurve = new List<AnimationCurve>(3);
	public float value = 0;
	public float temperature = 500;
	public float minTemperature = 100f;
	public float maxTemperature = 1000f;
	public float tempPerSecond = 2f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		temperature = Mathf.Clamp(temperature + tempPerSecond * Time.deltaTime, minTemperature, maxTemperature);

		temperatureDisplay.text = temperature.ToString("n1") + tempString;
		Color c = Color.white;
		value = Mathf.InverseLerp(minTemperature, maxTemperature, temperature);
		c.r = colorCurve[0].Evaluate(Mathf.InverseLerp(minTemperature, maxTemperature, temperature));
		c.g = colorCurve[1].Evaluate(Mathf.InverseLerp(minTemperature, maxTemperature, temperature));
		c.b = colorCurve[2].Evaluate(Mathf.InverseLerp(minTemperature, maxTemperature, temperature));
		temperatureDisplay.GetComponent<Renderer>().material.color = c;

		if (temperature >= maxTemperature)
		{
			if (GameManager.Instance)
			{
				GameManager.Instance.GameOver();
			}
		}
	}
}
