﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class HomoPrinter : MonoBehaviour {

	public Transform printerPoint;
	public List<HomoPrinterItem> items = new List<HomoPrinterItem>();
	int currentIndex = 0;
	RawImage image;

	// Use this for initialization
	void Start () {
		image = GetComponentInChildren<RawImage>();
	}

	public void Back()
	{
		currentIndex--;
		if (currentIndex < 0)
		{
			currentIndex = items.Count - 1;
		}
		UpdateUI();
	}

	public void Next()
	{
		currentIndex++;
		if (currentIndex >= items.Count)
		{
			currentIndex = 0;
		}
		UpdateUI();
	}

	public void Print()
	{
		if (items[currentIndex].prefab)
		{
			Instantiate(items[currentIndex].prefab, printerPoint.position, Quaternion.identity);
		}
	}

	void UpdateUI()
	{
		image.texture = items[currentIndex].image;
	}
}
[System.Serializable]
public class HomoPrinterItem
{
	public GameObject prefab;
	public Texture image;
}
