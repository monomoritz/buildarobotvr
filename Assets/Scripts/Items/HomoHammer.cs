﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class HomoHammer : MonoBehaviour {

	public AudioSource sounds;
	public OnTriggerMessage collisionTrigger;
	public float minVelocity = 5f;

	float velocity;
	Vector3 oldPos;

	// Use this for initialization
	private void OnEnable () {
		if (collisionTrigger)
		{
			collisionTrigger.onTriggerEnterEvent += OnOtherCollisionEnter;
		}
		oldPos = collisionTrigger.transform.position;
	}

	private void OnDisable()
	{
		if (collisionTrigger)
		{
			collisionTrigger.onTriggerEnterEvent -= OnOtherCollisionEnter;
		}
	}

	private void Update()
	{
		var newPos = collisionTrigger.transform.position;
		var media = (newPos - oldPos);
		velocity = (media / Time.deltaTime).magnitude;
		oldPos = newPos;
	}

	private void OnOtherCollisionEnter(Collider other)
	{
		var snapable = other.gameObject.GetComponent<FreeSnapable>();
		if (snapable && snapable.snapper != null && velocity > minVelocity)
		{
			if (snapable is FreeWeldable)
			{
				snapable.GetComponent<FreeWeldable>().weldValue = 0f;
			}
			snapable.Unsnap();
			GetComponent<VRTK_InteractHaptics>().HapticsOnUse(GetComponent<VRTK_InteractableObject>().GetGrabbingObject().GetComponent<Homo_InteractUse>().GetControllerReference()); // Feedback
			if (sounds)
			{
				sounds.PlayOneShot(sounds.clip);
			}
		}
	}
}
