﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

[RequireComponent(typeof(VRTK_InteractableObject))]
public class HomoWelder : MonoBehaviour
{

	public AudioSource sounds;
	public OnTriggerMessage trigger;
	public GameObject display;
	public Transform progressBar;
	public List<ParticleSystem> particles = new List<ParticleSystem>();

	float velocity;
	Vector3 oldPos;

	List<GameObject> controllers;
	VRTK_InteractableObject interactable;
	FreeWeldable weldable;
	GameObject weldController;
	private bool isWelding = false;

	#region Events
	private void OnEnable()
	{
		if (trigger)
		{
			trigger.onTriggerEnterEvent += OnOtherTriggerEnter;
			trigger.onTriggerExitEvent += OnOtherTriggerExit;
		}

		interactable = GetComponent<VRTK_InteractableObject>();
		interactable.InteractableObjectUsed += StartWeld;
		interactable.InteractableObjectUnused += StopWeld;
	}

	private void OnDisable()
	{
		if (trigger)
		{
			trigger.onTriggerEnterEvent -= OnOtherTriggerEnter;
			trigger.onTriggerExitEvent -= OnOtherTriggerExit;
		}

		interactable.InteractableObjectUsed -= StartWeld;
		interactable.InteractableObjectUnused -= StopWeld;
	}
	#endregion

	private void StartWeld(object sender, InteractableObjectEventArgs e)
	{
		weldController = e.interactingObject;
		isWelding = true;
	}

	private void StopWeld(object sender, InteractableObjectEventArgs e)
	{
		isWelding = false;
	}

	private void Update()
	{
		if (isWelding && weldable && (weldable.weldValue < 1f))
		{
			if (weldable && weldable.IsSnapped())
			{
				weldable.Weld(weldController);
				if (display)
				{
					if (!display.activeInHierarchy)
					{
						display.SetActive(true);
					}
				}
				if (progressBar)
				{
					progressBar.localScale = new Vector3(weldable.weldValue, progressBar.localScale.y, progressBar.localScale.z);
				}
			}
			
			foreach (var ps in particles)
			{
				if (!ps.isPlaying)
					ps.Play();
			}

			if (sounds && !sounds.isPlaying)
			{
				sounds.Play();
			}

			GetComponent<VRTK_InteractHaptics>().HapticsOnUse(weldController.GetComponent<Homo_InteractUse>().GetControllerReference()); // Feedback
		}
		else
		{
			if (display)
			{
				if (display.activeInHierarchy)
				{
					display.SetActive(false);
				}

				foreach (var ps in particles)
				{
					if (ps.isPlaying)
						ps.Stop();
				}

				if (sounds && sounds.isPlaying)
				{
					sounds.Stop();
				}
			}
		}
	}


	private void OnOtherTriggerEnter(Collider other)
	{
		var w = other.GetComponent<FreeWeldable>();
		if (w && w.IsSnapped())
		{
			weldable = w;
		}
	}

	private void OnOtherTriggerExit(Collider other)
	{
		var w = other.GetComponent<FreeWeldable>();
		if (w == weldable)
		{
			weldable = null;
		}
	}
}
