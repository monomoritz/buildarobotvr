﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class HomoSpray : MonoBehaviour
{
	public float tempPerSecond = 10f;
	public List<ParticleSystem> water = new List<ParticleSystem>();
	public AudioSource sounds;
	public OnTriggerMessage trigger;
	public GameObject display;
	public Transform progressBar;
	protected VRTK_InteractableObject interactable;
	protected GameObject controller;
	protected VRTK_ControllerEvents events;
	RobotCore roboCore;

	// Use this for initialization
	void Start()
	{
		interactable = GetComponent<VRTK_InteractableObject>();
		trigger.gameObject.SetActive(false);
		trigger.onTriggerStayEvent += CoreInTrigger;
	}

	private void OnEnable()
	{
		StartCoroutine(EOnEnable());
	}

	IEnumerator EOnEnable()
	{
		yield return null;
		interactable.InteractableObjectGrabbed += Grabbed;
		interactable.InteractableObjectUngrabbed += Ungrabbed;
	}

	private void OnDisable()
	{
		interactable.InteractableObjectGrabbed -= Ungrabbed;
		interactable.InteractableObjectUngrabbed -= Grabbed;
	}

	private void Grabbed(object sender, InteractableObjectEventArgs e)
	{
		controller = e.interactingObject;
		events = controller.GetComponent<VRTK_ControllerEvents>();
		controller.GetComponent<VRTK_ControllerEvents>().TriggerHairlineStart += Use;
		controller.GetComponent<VRTK_ControllerEvents>().TriggerHairlineEnd += Unuse;
	}

	private void Ungrabbed(object sender, InteractableObjectEventArgs e)
	{
		controller.GetComponent<VRTK_ControllerEvents>().TriggerHairlineStart -= Use;
		controller.GetComponent<VRTK_ControllerEvents>().TriggerHairlineEnd -= Unuse;
		controller = null;
		events = null;
	}

	private void Use(object sender, ControllerInteractionEventArgs e)
	{
		if (water.Count > 0)
		{
			foreach (var item in water)
			{
				item.Play();
			}
		}
		trigger.gameObject.SetActive(true);
		if (display)
		{
			if (!display.activeInHierarchy)
			{
				display.SetActive(true);
			}
		}
		if (sounds)
		{
			sounds.Play();
		}
	}

	private void Unuse(object sender, ControllerInteractionEventArgs e)
	{
		if (water.Count > 0)
		{
			foreach (var item in water)
			{
				item.Stop();
			}
		}
		trigger.gameObject.SetActive(false);
		if (display)
		{
			if (display.activeInHierarchy)
			{
				display.SetActive(false);
			}
		}
		if (progressBar)
		{
			progressBar.localScale = new Vector3(0f, progressBar.localScale.y, progressBar.localScale.z);
		}
		if (sounds)
		{
			sounds.Stop();
		}
	}

	// Update is called once per frame
	void Update()
	{
		if (controller && water.Count > 0)
		{
			float v = events.GetTriggerAxis();
			float speedMin = Mathf.Lerp(1f, 10f, v);
			float speedMax = Mathf.Lerp(2f, 20f, v);
			foreach (var item in water)
			{
				var main = item.main;
				main.startSpeed = new ParticleSystem.MinMaxCurve(speedMin, speedMax);
			}

			if (display && display.activeInHierarchy && roboCore && progressBar)
			{
				progressBar.localScale = new Vector3(Mathf.InverseLerp(roboCore.minTemperature, roboCore.maxTemperature, roboCore.temperature), progressBar.localScale.y, progressBar.localScale.z);
			}
		}
	}

	private void CoreInTrigger(Collider other)
	{
		roboCore = other.GetComponent<RobotCore>();
		if (roboCore)
		{
			roboCore.temperature -= tempPerSecond * Time.deltaTime;
		}
	}
}
