﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(FreeWeldable))]
public class HomoPlayer : MonoBehaviour
{

	public List<AudioClip> songs = new List<AudioClip>();
	int currentSong = -1;

	AudioSource audioSource;

	private void Start()
	{
		audioSource = GetComponent<AudioSource>();
		audioSource.loop = false;
	}

	public void Next()
	{
		//if (!GetComponent<FreeWeldable>().isFixed)
		//	return;

		currentSong++;
		if (currentSong >= songs.Count)
		{
			currentSong = -1;
			audioSource.clip = null;
		}
		else
		{
			audioSource.clip = songs[currentSong];
			audioSource.Play();
		}
	}
}
